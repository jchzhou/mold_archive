Name:           mold
Version:        1.6.0
Release:        1
Summary:        A Modern Linker (mold)
License:        AGPLv3+
URL:            https://github.com/rui314/mold
Source:         https://github.com/rui314/mold/archive/v%{version}/mold-%{version}.tar.gz

# mold currently supports x86-64, i386, ARM32, ARM64 and 64-bit RISC-V.
ExclusiveArch:  x86_64 aarch64 riscv64
BuildRequires:  cmake
BuildRequires:  gcc
BuildRequires:  gcc-c++ >= 10
BuildRequires:  openssl-devel
BuildRequires:  python3
BuildRequires:  xxhash-devel
BuildRequires:  zlib-devel
BuildRequires:  libzstd
BuildRequires:  libzstd-devel

# following pkgs are only required for the test
BuildRequires:  clang
BuildRequires:  gdb
BuildRequires:  glibc-devel
BuildRequires:  libstdc++-static
BuildRequires:  libdwarf-tools
BuildRequires:  llvm
BuildRequires:  perl
# API-incompatible with older tbb 2020.3 currently shipped by openEuler
Provides:   bundled(tbb) = 2021.5

Patch0:     tbb-strip-werror.patch

# Allow building against the system-provided `xxhash.h`
Patch1:     0001-Use-system-compatible-include-path-for-xxhash.h.patch

%define build_args PREFIX=%{_prefix} LIBDIR=%{_libdir} CFLAGS="%{build_cflags}" CXXFLAGS="%{build_cxxflags} -Wno-sign-compare" LDFLAGS="%{build_ldflags}" STRIP=true SYSTEM_XXHASH=1 USE_MIMALLOC=0 SYSTEM_ZSTD=1

%description
mold is a faster drop-in replacement for existing Unix linkers.
It is several times faster than the LLVM lld linker.
mold is designed to increase developer productivity by reducing
build time, especially in rapid debug-edit-rebuild cycles.

%prep
%autosetup -p1
rm -r third-party/xxhash

%build
%make_build %{build_args}

%install
%make_install %{build_args}
chmod +x %{buildroot}%{_libdir}/mold/mold-wrapper.so

%check
%ifnarch riscv64
make test -k -e %{build_args}
%endif

%files
%license %{_docdir}/mold/LICENSE
%{_bindir}/mold
%{_bindir}/ld.mold
%{_bindir}/ld64.mold
%{_libdir}/mold/mold-wrapper.so
%{_libexecdir}/mold
%{_libexecdir}/mold/ld
%{_mandir}/man1/ld.mold.1*
%{_mandir}/man1/mold.1*

%changelog
* Wed Oct 19 2022 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.6.0-1
- Update to 1.6.0

* Mon Aug 22 2022 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.5.0-1
- Update to 1.5.0, add zstd as dependency

* Mon Aug 22 2022 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.4.1-1
- Bump version to 1.4.1, minor cleanup

* Tue Jul 05 2022 jchzhou <zhoujiacheng@iscas.ac.cn> - 1.3.1-1
- Init package from fedora
- Credit: Christoph Erhardt (fedora@sicherha.de)
- Link: https://src.fedoraproject.org/rpms/mold